<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Entity\Product;

// Controller created with dynamic routing.
// You can easly setup your own routing with @route annotation
class ProductController extends Controller implements ClassResourceInterface
{
    /**
     * @Rest\View(serializerGroups={"list"})
     */
    public function cgetAction()
    {
        return $this->get('product.manager')->getProducts();
    }
    
    /**
     * @Rest\View(serializerGroups={"details"})
     */
    public function getAction(Product $product) 
    {   
        return $product;
    }
    
    public function postAction(Request $request) 
    {
        $this->get('product.manager')->addProduct($request->request->get('product'));
                    
        return new Response('Product added successfully');
    }
    
    public function putAction(Product $product) 
    {
        $this->get('product.manager')->updateProduct($product, $request->request->get('product'));

        return new Response('Product "' . $product . '" updated successfully.');
    }
    
    public function deleteAction(Product $product) 
    {
        $this->get('product.manager')->deleteProduct($product);

        return new Response('Product ' . $product . ' deleted successfully');
    }
}