<?php

namespace AppBundle\Manager;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Inject;
use AppBundle\Entity\Product;


/**
 * @Service("product.manager")
 */
class ProductManager
{
    /**
     * @Inject("doctrine.orm.entity_manager")
     */
    public $em;

    public function getProducts() {
        return $this->em
        ->getRepository('AppBundle:Product')
        ->findAll();
    }

    public function getProduct(Int $id) {
        return $this->em
        ->getRepository('AppBundle:Product')
        ->find($request->get('id'));
    }
    
    public function addProduct($data) {
        $product = new Product();
        $product
            ->setName($data['name'])
            ->setPrice($data['price'])
            ->setDescription($data['description'])
        ;
        $this->em->persist($product);
        $this->em->flush();
    }
    
    public function updateProduct($product, $data) {
        $product
            ->setName($data['name'])
            ->setPrice($data['price'])
            ->setDescription($data['description'])
        ;
        $this->em->persist($product);
        $this->em->flush();
    }
    
    public function deleteProduct($product) {
        $this->em->remove($product);
        $this->em->flush();
    }
}